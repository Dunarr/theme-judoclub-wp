<?php
get_header();
?>
<div class="container">
    <h1>Les photos du club</h1>
        <?php
        if (have_posts()) {
            while (have_posts()) {
                the_post();
                ?>
                <a href="<?= get_permalink() ?>" class="galerie-thumbnail">
                    <?= get_the_post_thumbnail() ?>
                    <div class="title-shadow container"><h2><?= get_the_title(); ?></h2></div>
                </a>
                <?php
            }
        }
        ?>
</div>
<?php
get_footer();
?>
