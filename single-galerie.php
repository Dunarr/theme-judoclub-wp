<?php
get_header();
?>
<div class="container">
    <?php
    if (have_posts()) {
        while (have_posts()) {
            the_post();
            ?>
            <h1><?= get_the_title() ?></h1>
            <p><?= get_field("description") ?></p>
           <div class="row">
               <?php foreach( get_field("images") as $image ): ?>
                   <div class="col s12 m6 l4 galerie-image-thumbnail">
                       <img class="materialboxed" src="<?= $image['sizes']['large'] ?>" alt="<?= $image['alt'] ?>">
                   </div>
               <?php endforeach; ?>
           </div>
            <?php
        }
    }
    ?>
</div>
<?php
get_footer();
?>
