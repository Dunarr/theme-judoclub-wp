<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Judo Club Sargé</title>
    <link rel="icon" href="<?= get_stylesheet_directory_uri() ?>/assets/favicon.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <?php
        wp_head();
    ?>
</head>
<body>

<nav>
    <div class="nav-wrapper grey darken-3">
        <a href="/" class="brand-logo">
            <img src="<?= get_stylesheet_directory_uri() ?>/assets/Logo-sarge-3.png" alt="">
        </a>
        <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
        <?php
        wp_nav_menu(["theme_location"=>"primary", "menu_class"=>"right hide-on-med-and-down light-blue-text text-darken-4"])
        ?>
    </div>
</nav>

<?php
wp_nav_menu(["theme_location"=>"primary", "menu_class"=>"sidenav", "menu_id"=>"mobile-demo"])
?>

