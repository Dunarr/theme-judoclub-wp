<?php
get_header();
?>
<div class="container">
    <h1>Les documents importants</h1>
    <div class="row">
        <?php
        if (have_posts()) {
        while (have_posts()) {
        the_post();
        ?>
        <a download="<?= get_field("document")['filename']; ?>" href="<?= get_field("document")['url']; ?>" class="col s12 l6">
            <div class="dl-card card-panel light-blue darken-4">
                <div class="dl-card-icon">
                    <i class="medium material-icons">file_download</i>
                </div>
                <h2 class="white-text"><?= get_the_title(); ?></h2>
            </div>
    </div>


    <?php
    }
    }
    ?>
</div>
<?php
get_footer();
?>
