import 'materialize-css/dist/css/materialize.css';
// import * as $ from 'jQuery';
import * as M from 'materialize-css';

document.addEventListener('DOMContentLoaded', () => {
  const sidenav = document.querySelectorAll('.sidenav');
  M.Sidenav.init(sidenav);

  const materialboxed = document.querySelectorAll('.materialboxed');
  M.Materialbox.init(materialboxed);
});
