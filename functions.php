<?php
add_action("wp_enqueue_scripts", "add_styles");
add_action( 'after_setup_theme', 'register_judo_menu');
add_action("init", "judo_theme_options");
add_filter('nav_menu_css_class' , 'materialize_nav_class', 10 , 2);
add_action( 'admin_menu', 'custom_menu_page_removing' );

function add_styles() {
    require_once "dist/imports.php";
}

function register_judo_menu() {
    register_nav_menu( 'primary', __( 'Menu principal', 'judoclub' ) );
}

function judo_theme_options() {
    acf_add_options_page( [
        'page_title' => __('Options accueuil'),
        'menu_slug' => 'options-accueuil',
        'post_id' => 'home-options',
        'capability' => 'edit_posts',
        'position' => 4,
        'icon_url' => "
dashicons-admin-home",
        ] );
}

function materialize_nav_class ($classes, $item) {
    if (in_array('current-menu-item', $classes) ){
        $classes[] = 'active ';
    }
    return $classes;
}
function custom_menu_page_removing() {
    remove_menu_page( "edit-comments.php" );
}
