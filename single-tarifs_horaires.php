<?php
get_header();
?>
<div class="container">
    <?php
    if (have_posts()) {
        while (have_posts()) {
            the_post();
            ?>
            <h1><?= get_the_title() ?></h1>
            <div class="wysiyg"><?= get_field('contenu_haut') ?></div>
            <h2>Horaires</h2>
            <table>
                <thead>
                <tr>
                    <th>Groupe</th>
                    <th>Jour</th>
                    <th>Horaires</th>
                </tr>
                </thead>
                <?php
                foreach (get_field('horaires') as $horaire) {
                    ?>
                    <tr class="<?= empty($horaire['groupe'])?'':'border-top' ?>">
                        <td><?= $horaire["groupe"] ?></td>
                        <td><?= $horaire["jour"] ?></td>
                        <td><?= $horaire["horaires"] ?></td>
                    </tr>
                    <?php
                }
                ?>
            </table>
            <h2>Tarifs</h2>
            <table>
                <thead>
                <tr>
                    <th>Groupe</th>
                    <th>Année</th>
                    <th>Licence</th>
                    <th>Sargé/Yvré</th>
                    <th>Hors Sargé/Yvré</th>
                </tr>
                </thead>
                <?php
                foreach (get_field('tarifs') as $tarif) {
                    ?>
                    <tr>
                        <td><?= $tarif["groupe"] ?></td>
                        <td><?= $tarif["age"] ?></td>
                        <td><?= $tarif["licence"] ?>€</td>
                        <td><?= $tarif["sarge_yvre"] ?>€</td>
                        <td><?= $tarif["hors_sarge_yvre"] ?>€</td>
                    </tr>
                    <?php
                }
                ?>
            </table>


            <div class="wysiyg"><?= get_field('contenu_bas') ?></div>
            <?php
        }
    }
    ?>
</div>
<?php
get_footer();
?>
